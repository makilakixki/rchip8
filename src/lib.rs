#![no_std]

mod cpu;
mod error;
mod keyboard;
mod ram;
mod screen;
mod stack;

use core::time::Duration;

use cpu::Cpu;
use keyboard::Keyboard;
use ram::Ram;
use screen::Screen;
use stack::Stack;

pub use cpu::Opcode;
pub use error::Chip8Error;
pub use keyboard::Keys;
pub use screen::{SCREEN_HEIGHT, SCREEN_WIDTH};

const ROM_ADDR: usize = 0x200;

const FONT_SPRITES: [u8; 80] = [
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x20, 0x60, 0x20, 0x20, 0x70, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80, // F
];

pub struct Chip8 {
    cpu: Cpu,
    stack: Stack,
    ram: Ram,
    keyboard: Keyboard,
    screen: Screen,
    prev_timer_time: u128,
    prev_instr_time: u128,
    timer_time_window: Duration,
    instr_time_window: Duration,
}

impl Chip8 {
    pub fn new(instr_per_sec: u64) -> Self {
        let mut ram = Ram::new();
        ram.load_data(0, &FONT_SPRITES);
        Self {
            cpu: Cpu::new(ROM_ADDR),
            stack: Stack::new(),
            ram,
            keyboard: Keyboard::new(),
            screen: Screen::new(),
            prev_timer_time: 0_u128,
            prev_instr_time: 0_u128,
            timer_time_window: Duration::from_nanos(1_000_000_000 / 60), // 60 Hz time window
            instr_time_window: Duration::from_nanos(1_000_000_000 / instr_per_sec),
        }
    }

    pub fn reset(&mut self) {
        self.cpu.reset(ROM_ADDR);
        self.screen.clear();
    }

    pub fn load_rom(&mut self, rom: &[u8]) -> Result<(), Chip8Error> {
        if ROM_ADDR + rom.len() < self.ram.size() {
            self.ram.load_data(ROM_ADDR, rom);
            return Ok(());
        }
        Err(Chip8Error::FileTooLarge)
    }

    pub fn tick(&mut self, nanosec: u128) -> Result<(Option<Opcode>, Option<bool>), Chip8Error> {
        let mut beep = None;
        let mut opcode = None;

        let prev_timer_duration =
            Duration::from_nanos((nanosec - self.prev_timer_time).try_into()?);
        let prev_instr_duration =
            Duration::from_nanos((nanosec - self.prev_instr_time).try_into()?);

        if prev_timer_duration > self.timer_time_window {
            beep = Some(self.cpu.tick_timers());
            self.prev_timer_time = nanosec;
        }
        if prev_instr_duration > self.instr_time_window {
            opcode = Some(self.cpu.tick_instruction(
                &mut self.stack,
                &mut self.ram,
                &self.keyboard,
                &mut self.screen,
            )?);
            self.prev_instr_time = nanosec;
        }
        Ok((opcode, beep))
    }

    pub const fn get_screen_buf(&self) -> &[bool] {
        self.screen.buff.as_slice()
    }

    pub const fn get_pixel(&self, i: usize, j: usize) -> bool {
        self.screen.buff[i + (j * SCREEN_WIDTH)]
    }

    pub const fn get_screen_update_flag(&self) -> bool {
        self.screen.update_flag
    }

    pub fn set_screen_as_drawn(&mut self) {
        self.screen.update_flag = false;
    }

    pub fn press_key(&mut self, key: Keys) {
        self.keyboard.key_buf[usize::from(key)] = true;
    }

    pub fn release_key(&mut self, key: Keys) {
        self.keyboard.key_buf[usize::from(key)] = false;
    }

    // Debug utilities

    pub const fn get_v_regs(&self) -> &[u8] {
        self.cpu.v_regs.as_slice()
    }

    pub const fn get_i_reg(&self) -> u16 {
        self.cpu.i_reg
    }

    pub const fn get_dt(&self) -> u8 {
        self.cpu.dt
    }

    pub const fn get_st(&self) -> u8 {
        self.cpu.dt
    }

    pub const fn get_pc(&self) -> usize {
        self.cpu.pc
    }

    pub const fn get_sp(&self) -> u8 {
        self.cpu.sp
    }
}
