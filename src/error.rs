use core::{fmt, num::TryFromIntError};

#[derive(Debug)]
pub enum Chip8Error {
    FileTooLarge,
    FailedIntConversion,
}

impl core::error::Error for Chip8Error {}

impl fmt::Display for Chip8Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            Chip8Error::FileTooLarge => write!(f, "File too large"),
            Chip8Error::FailedIntConversion => write!(f, "Integral type conversion failed"),
        }
    }
}

impl From<TryFromIntError> for Chip8Error {
    fn from(_: TryFromIntError) -> Self {
        Self::FailedIntConversion
    }
}
