const STACK_SIZE: usize = 0x10;

pub(super) struct Stack {
    stack: [usize; STACK_SIZE],
}

impl Stack {
    pub(super) const fn new() -> Self {
        Self {
            stack: [0; STACK_SIZE],
        }
    }

    pub(super) fn pop(&self, sp: u8) -> usize {
        self.stack[usize::from(sp)]
    }

    pub(super) fn push(&mut self, sp: u8, data: usize) {
        self.stack[usize::from(sp)] = data;
    }
}
