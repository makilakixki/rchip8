const KEY_COUNT: usize = 16;

pub enum Keys {
    K0,
    K1,
    K2,
    K3,
    K4,
    K5,
    K6,
    K7,
    K8,
    K9,
    KA,
    KB,
    KC,
    KD,
    KE,
    KF,
}

impl From<Keys> for usize {
    fn from(keys: Keys) -> Self {
        match keys {
            Keys::K0 => 0x0,
            Keys::K1 => 0x1,
            Keys::K2 => 0x2,
            Keys::K3 => 0x3,
            Keys::K4 => 0x4,
            Keys::K5 => 0x5,
            Keys::K6 => 0x6,
            Keys::K7 => 0x7,
            Keys::K8 => 0x8,
            Keys::K9 => 0x9,
            Keys::KA => 0xA,
            Keys::KB => 0xB,
            Keys::KC => 0xC,
            Keys::KD => 0xD,
            Keys::KE => 0xE,
            Keys::KF => 0xF,
        }
    }
}

pub(super) struct Keyboard {
    pub(super) key_buf: [bool; KEY_COUNT],
}

impl Keyboard {
    pub(super) const fn new() -> Self {
        Self {
            key_buf: [false; KEY_COUNT],
        }
    }

    pub(super) fn check_keypress(&self) -> Option<usize> {
        (0..self.key_buf.len()).find(|&i| self.key_buf[i])
    }
}
