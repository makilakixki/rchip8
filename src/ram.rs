const RAM_SIZE: usize = 0x1000;

pub(super) struct Ram {
    pub(super) buff: [u8; RAM_SIZE],
}

impl Ram {
    pub(super) const fn new() -> Self {
        Self {
            buff: [0; RAM_SIZE],
        }
    }

    pub(super) const fn size(&self) -> usize {
        self.buff.len()
    }

    pub(super) fn load_data(&mut self, start: usize, data: &[u8]) {
        let end = start + data.len();
        self.buff[start..end].copy_from_slice(data);
    }
}
