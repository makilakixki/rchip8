use core::fmt;

use rand::random;

use crate::{
    keyboard::Keyboard,
    ram::Ram,
    screen::{Screen, SCREEN_HEIGHT, SCREEN_WIDTH},
    stack::Stack,
    Chip8Error,
};

const REG_COUNT: usize = 0x10;

pub(super) struct Cpu {
    pub(super) v_regs: [u8; REG_COUNT],
    pub(super) i_reg: u16,
    pub(super) dt: u8,
    pub(super) st: u8,
    pub(super) pc: usize,
    pub(super) sp: u8,
}

impl Cpu {
    pub(super) const fn new(pc_pos: usize) -> Self {
        Self {
            v_regs: [0; REG_COUNT],
            i_reg: 0,
            dt: 0,
            st: 0,
            pc: pc_pos,
            sp: 0,
        }
    }

    pub(super) fn reset(&mut self, pc_pos: usize) {
        *self = Self::new(pc_pos);
    }

    pub(super) fn tick_timers(&mut self) -> bool {
        if self.dt > 0 {
            self.dt -= 1;
        }
        if self.st > 0 {
            self.st -= 1;
        }
        self.st > 0
    }

    pub(super) fn tick_instruction(
        &mut self,
        stack: &mut Stack,
        ram: &mut Ram,
        keyboard: &Keyboard,
        screen: &mut Screen,
    ) -> Result<Opcode, Chip8Error> {
        let opcode = u16::from(ram.buff[self.pc]) << 8 | u16::from(ram.buff[self.pc + 1]);

        let dig1: u8 = ((opcode & 0xF000) >> 12).try_into()?;
        let dig2: usize = ((opcode & 0x0F00) >> 8).into();
        let dig3: usize = ((opcode & 0x00F0) >> 4).into();
        let dig4: u8 = (opcode & 0x000F).try_into()?;

        self.pc += 2;

        match (dig1, dig2, dig3, dig4) {
            (0x0, 0x0, 0x0, 0x0) => Ok(Opcode::NoOp),
            (0x0, 0x0, 0xE, 0x0) => Ok(self.op_00e0(screen)),
            (0x0, 0x0, 0xE, 0xE) => Ok(self.op_00ee(stack)),
            (0x1, _, _, _) => Ok(self.op_1nnn(opcode)),
            (0x2, _, _, _) => Ok(self.op_2nnn(stack, opcode)),
            (0x3, _, _, _) => Ok(self.op_3xnn(dig2, opcode)),
            (0x4, _, _, _) => Ok(self.op_4xnn(dig2, opcode)),
            (0x5, _, _, 0x0) => Ok(self.op_5xy0(dig2, dig3)),
            (0x6, _, _, _) => self.op_6xnn(dig2, opcode),
            (0x7, _, _, _) => self.op_7xnn(dig2, opcode),
            (0x8, _, _, 0x0) => Ok(self.op_8xy0(dig2, dig3)),
            (0x8, _, _, 0x1) => Ok(self.op_8xy1(dig2, dig3)),
            (0x8, _, _, 0x2) => Ok(self.op_8xy2(dig2, dig3)),
            (0x8, _, _, 0x3) => Ok(self.op_8xy3(dig2, dig3)),
            (0x8, _, _, 0x4) => Ok(self.op_8xy4(dig2, dig3)),
            (0x8, _, _, 0x5) => Ok(self.op_8xy5(dig2, dig3)),
            (0x8, _, _, 0x6) => Ok(self.op_8xy6(dig2)),
            (0x8, _, _, 0x7) => Ok(self.op_8xy7(dig2, dig3)),
            (0x8, _, _, 0xE) => Ok(self.op_8xye(dig2)),
            (0x9, _, _, 0x0) => Ok(self.op_9xy0(dig2, dig3)),
            (0xA, _, _, _) => Ok(self.op_annn(opcode)),
            (0xB, _, _, _) => Ok(self.op_bnnn(opcode)),
            (0xC, _, _, _) => self.op_cxnn(dig2, opcode),
            (0xD, _, _, _) => Ok(self.op_dxyn(ram, screen, dig2, dig3, dig4)),
            (0xE, _, 0x9, 0xE) => Ok(self.op_ex9e(keyboard, dig2)),
            (0xE, _, 0xA, 0x1) => Ok(self.op_exa1(keyboard, dig2)),
            (0xF, _, 0x0, 0x7) => Ok(self.op_fx07(dig2)),
            (0xF, _, 0x0, 0xA) => self.op_fx0a(keyboard, dig2),
            (0xF, _, 0x1, 0x5) => Ok(self.op_fx15(dig2)),
            (0xF, _, 0x1, 0x8) => Ok(self.op_fx18(dig2)),
            (0xF, _, 0x1, 0xE) => Ok(self.op_fx1e(dig2)),
            (0xF, _, 0x2, 0x9) => Ok(self.op_fx29(dig2)),
            (0xF, _, 0x3, 0x3) => Ok(self.op_fx33(ram, dig2)),
            (0xF, _, 0x5, 0x5) => Ok(self.op_fx55(ram, dig2)),
            (0xF, _, 0x6, 0x5) => Ok(self.op_fx65(ram, dig2)),
            (_, _, _, _) => unimplemented!(
                "Unimplemented opcode: {opcode:#0X}. Specified file probably is not a Chip8 ROM."
            ),
        }
    }
}

// Opcode definition
pub enum Opcode {
    NoOp,
    Op00E0,
    Op00EE,
    Op1NNN { nnn: u16 },
    Op2NNN { nnn: u16 },
    Op3XNN { x: usize, nn: u16 },
    Op4XNN { x: usize, nn: u16 },
    Op5XY0 { x: usize, y: usize },
    Op6XNN { x: usize, nn: u8 },
    Op7XNN { x: usize, nn: u16 },
    Op8XY0 { x: usize, y: usize },
    Op8XY1 { x: usize, y: usize },
    Op8XY2 { x: usize, y: usize },
    Op8XY3 { x: usize, y: usize },
    Op8XY4 { x: usize, y: usize },
    Op8XY5 { x: usize, y: usize },
    Op8XY6 { x: usize },
    Op8XY7 { x: usize, y: usize },
    Op8XYE { x: usize },
    Op9XY0 { x: usize, y: usize },
    OpANNN { nnn: u16 },
    OpBNNN { nnn: u16 },
    OpCXNN { x: usize, nn: u8 },
    OpDXYN { x: usize, y: usize, n: u8 },
    OpEX9E { x: usize },
    OpEXA1 { x: usize },
    OpFX07 { x: usize },
    OpFX0A { x: usize },
    OpFX15 { x: usize },
    OpFX18 { x: usize },
    OpFX1E { x: usize },
    OpFX29 { x: usize },
    OpFX33 { x: usize },
    OpFX55 { x: usize },
    OpFX65 { x: usize },
}

impl fmt::Display for Opcode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            Opcode::NoOp => write!(f, "NOP"),
            Opcode::Op00E0 => write!(f, "CLS"),
            Opcode::Op00EE => write!(f, "RET"),
            Opcode::Op1NNN { nnn } => write!(f, "JP   #{nnn:#05X}"),
            Opcode::Op2NNN { nnn } => write!(f, "CALL #{nnn:#05X}"),
            Opcode::Op3XNN { x, nn } => write!(f, "SE   V{x:X}, #{nn}"),
            Opcode::Op4XNN { x, nn } => write!(f, "SNE  V{x:X}, #{nn}"),
            Opcode::Op5XY0 { x, y } => write!(f, "SE   V{x:X}, V{y:X}"),
            Opcode::Op6XNN { x, nn } => write!(f, "LD   V{x:X}, #{nn}"),
            Opcode::Op7XNN { x, nn } => write!(f, "ADD  V{x:X}, #{nn}"),
            Opcode::Op8XY0 { x, y } => write!(f, "LD   V{x:X}, V{y:X}"),
            Opcode::Op8XY1 { x, y } => write!(f, "OR   V{x:X}, V{y:X}"),
            Opcode::Op8XY2 { x, y } => write!(f, "AND  V{x:X}, V{y:X}"),
            Opcode::Op8XY3 { x, y } => write!(f, "XOR  V{x:X}, V{y:X}"),
            Opcode::Op8XY4 { x, y } => write!(f, "ADD  V{x:X}, V{y:X}"),
            Opcode::Op8XY5 { x, y } => write!(f, "SUB  V{x:X}, V{y:X}"),
            Opcode::Op8XY6 { x } => write!(f, "SHR  V{x:X}"),
            Opcode::Op8XY7 { x, y } => write!(f, "SUBN V{x:X}, V{y:X}"),
            Opcode::Op8XYE { x } => write!(f, "SHL  V{x:X}"),
            Opcode::Op9XY0 { x, y } => write!(f, "SNE  V{x:X}, V{y:X}"),
            Opcode::OpANNN { nnn } => write!(f, "LD   I, #{nnn:#05X}"),
            Opcode::OpBNNN { nnn } => write!(f, "JP   V0, #{nnn:#05X}"),
            Opcode::OpCXNN { x, nn } => write!(f, "RND  V{x:X}, #{nn}"),
            Opcode::OpDXYN { x, y, n } => write!(f, "DRW  V{x:X}, V{y:X}, #{n}"),
            Opcode::OpEX9E { x } => write!(f, "SKP  V{x:X}"),
            Opcode::OpEXA1 { x } => write!(f, "SKNP V{x:X}"),
            Opcode::OpFX07 { x } => write!(f, "LD   V{x:X}, DT"),
            Opcode::OpFX0A { x } => write!(f, "LD   V{x:X}, K"),
            Opcode::OpFX15 { x } => write!(f, "LD   DT, V{x:X}"),
            Opcode::OpFX18 { x } => write!(f, "LD   ST, V{x:X}"),
            Opcode::OpFX1E { x } => write!(f, "ADD  I, V{x:X}"),
            Opcode::OpFX29 { x } => write!(f, "LD   F, V{x:X}"),
            Opcode::OpFX33 { x } => write!(f, "LD   B, V{x:X}"),
            Opcode::OpFX55 { x } => write!(f, "LD   [I], V{x:X}"),
            Opcode::OpFX65 { x } => write!(f, "LD   V{x:X}, [I]"),
        }
    }
}

// Opcode implementation
impl Cpu {
    fn op_00e0(&self, screen: &mut Screen) -> Opcode {
        screen.clear();
        screen.update_flag = true;
        Opcode::Op00E0
    }

    fn op_00ee(&mut self, stack: &Stack) -> Opcode {
        self.pc = stack.pop({
            self.sp -= 1;
            self.sp
        });
        Opcode::Op00EE
    }

    fn op_1nnn(&mut self, opcode: u16) -> Opcode {
        let nnn = opcode & 0x0FFF;
        self.pc = nnn.into();
        Opcode::Op1NNN { nnn }
    }

    fn op_2nnn(&mut self, stack: &mut Stack, opcode: u16) -> Opcode {
        stack.push(self.sp, self.pc);
        self.sp += 1;
        let nnn = opcode & 0x0FFF;
        self.pc = nnn.into();
        Opcode::Op2NNN { nnn }
    }

    fn op_3xnn(&mut self, x: usize, opcode: u16) -> Opcode {
        let nn = opcode & 0x00FF;
        if u16::from(self.v_regs[x]) == nn {
            self.pc += 2;
        }
        Opcode::Op3XNN { x, nn }
    }

    fn op_4xnn(&mut self, x: usize, opcode: u16) -> Opcode {
        let nn = opcode & 0x00FF;
        if u16::from(self.v_regs[x]) != nn {
            self.pc += 2;
        }
        Opcode::Op4XNN { x, nn }
    }

    fn op_5xy0(&mut self, x: usize, y: usize) -> Opcode {
        if self.v_regs[x] == self.v_regs[y] {
            self.pc += 2;
        }
        Opcode::Op5XY0 { x, y }
    }

    fn op_6xnn(&mut self, x: usize, opcode: u16) -> Result<Opcode, Chip8Error> {
        let nn: u8 = (opcode & 0x00FF).try_into()?;
        self.v_regs[x] = nn;
        Ok(Opcode::Op6XNN { x, nn })
    }

    fn op_7xnn(&mut self, x: usize, opcode: u16) -> Result<Opcode, Chip8Error> {
        let nn = opcode & 0x00FF;
        self.v_regs[x] = self.v_regs[x].wrapping_add(u8::try_from(nn)?);
        Ok(Opcode::Op7XNN { x, nn })
    }

    fn op_8xy0(&mut self, x: usize, y: usize) -> Opcode {
        self.v_regs[x] = self.v_regs[y];
        Opcode::Op8XY0 { x, y }
    }

    fn op_8xy1(&mut self, x: usize, y: usize) -> Opcode {
        self.v_regs[x] |= self.v_regs[y];
        Opcode::Op8XY1 { x, y }
    }

    fn op_8xy2(&mut self, x: usize, y: usize) -> Opcode {
        self.v_regs[x] &= self.v_regs[y];
        Opcode::Op8XY2 { x, y }
    }

    fn op_8xy3(&mut self, x: usize, y: usize) -> Opcode {
        self.v_regs[x] ^= self.v_regs[y];
        Opcode::Op8XY3 { x, y }
    }

    fn op_8xy4(&mut self, x: usize, y: usize) -> Opcode {
        let (wrapped_val, carry) = self.v_regs[x].overflowing_add(self.v_regs[y]);
        self.v_regs[x] = wrapped_val;
        self.v_regs[0xF] = carry.into();
        Opcode::Op8XY4 { x, y }
    }

    fn op_8xy5(&mut self, x: usize, y: usize) -> Opcode {
        self.v_regs[0xF] = (self.v_regs[x] > self.v_regs[y]).into();
        self.v_regs[x] = self.v_regs[x].wrapping_sub(self.v_regs[y]);
        Opcode::Op8XY5 { x, y }
    }

    fn op_8xy6(&mut self, x: usize) -> Opcode {
        self.v_regs[0xF] = self.v_regs[x] & 0x1;
        self.v_regs[x] >>= 1;
        Opcode::Op8XY6 { x }
    }

    fn op_8xy7(&mut self, x: usize, y: usize) -> Opcode {
        self.v_regs[0xF] = (self.v_regs[y] > self.v_regs[x]).into();
        self.v_regs[x] = self.v_regs[y].wrapping_sub(self.v_regs[x]);
        Opcode::Op8XY7 { x, y }
    }

    fn op_8xye(&mut self, x: usize) -> Opcode {
        self.v_regs[0xF] = self.v_regs[x] & (1 << 7);
        self.v_regs[x] <<= 1;
        Opcode::Op8XYE { x }
    }

    fn op_9xy0(&mut self, x: usize, y: usize) -> Opcode {
        if self.v_regs[x] != self.v_regs[y] {
            self.pc += 2;
        }
        Opcode::Op9XY0 { x, y }
    }

    fn op_annn(&mut self, opcode: u16) -> Opcode {
        let nnn = opcode & 0x0FFF;
        self.i_reg = nnn;
        Opcode::OpANNN { nnn }
    }

    fn op_bnnn(&mut self, opcode: u16) -> Opcode {
        let nnn = opcode & 0x0FFF;
        self.pc = (nnn + u16::from(self.v_regs[0])).into();
        Opcode::OpBNNN { nnn }
    }

    fn op_cxnn(&mut self, x: usize, opcode: u16) -> Result<Opcode, Chip8Error> {
        let nn: u8 = (opcode & 0x00FF).try_into()?;
        self.v_regs[x] = nn & random::<u8>();
        Ok(Opcode::OpCXNN { x, nn })
    }

    fn op_dxyn(&mut self, ram: &Ram, screen: &mut Screen, x: usize, y: usize, n: u8) -> Opcode {
        let mut flipped = false;
        for row in 0..n {
            let bits: u8 = ram.buff[usize::from(self.i_reg + u16::from(row))];

            for col in 0..8 {
                if bits & (1 << (7 - col)) != 0 {
                    let i = (usize::from(self.v_regs[x]) + col) % SCREEN_WIDTH;
                    let j = usize::from(self.v_regs[y] + row) % SCREEN_HEIGHT;

                    flipped |= screen.buff[i + (j * SCREEN_WIDTH)];
                    screen.buff[i + (j * SCREEN_WIDTH)] ^= true;
                }
            }
        }
        self.v_regs[0xF] = flipped.into();
        screen.update_flag = true;
        Opcode::OpDXYN { x, y, n }
    }

    fn op_ex9e(&mut self, keyboard: &Keyboard, x: usize) -> Opcode {
        if keyboard.key_buf[usize::from(self.v_regs[x])] {
            self.pc += 2;
        }
        Opcode::OpEX9E { x }
    }

    fn op_exa1(&mut self, keyboard: &Keyboard, x: usize) -> Opcode {
        if !keyboard.key_buf[usize::from(self.v_regs[x])] {
            self.pc += 2;
        }
        Opcode::OpEXA1 { x }
    }

    fn op_fx07(&mut self, x: usize) -> Opcode {
        self.v_regs[x] = self.dt;
        Opcode::OpFX07 { x }
    }

    fn op_fx0a(&mut self, keyboard: &Keyboard, x: usize) -> Result<Opcode, Chip8Error> {
        if let Some(key) = keyboard.check_keypress() {
            self.v_regs[x] = key.try_into()?;
        } else {
            self.pc -= 2;
        }

        Ok(Opcode::OpFX0A { x })
    }

    fn op_fx15(&mut self, x: usize) -> Opcode {
        self.dt = self.v_regs[x];
        Opcode::OpFX15 { x }
    }

    fn op_fx18(&mut self, x: usize) -> Opcode {
        self.st = self.v_regs[x];
        Opcode::OpFX18 { x }
    }

    fn op_fx1e(&mut self, x: usize) -> Opcode {
        self.i_reg = self.i_reg.wrapping_add(self.v_regs[x].into());
        Opcode::OpFX1E { x }
    }

    fn op_fx29(&mut self, x: usize) -> Opcode {
        self.i_reg = u16::from(self.v_regs[x]) * 5;
        Opcode::OpFX29 { x }
    }

    fn op_fx33(&mut self, ram: &mut Ram, x: usize) -> Opcode {
        let num = self.v_regs[x];
        let i: usize = self.i_reg.into();
        let h = num / 100;
        let t = (num - h * 100) / 10;
        let o = num - h * 100 - t * 10;
        ram.buff[i] = h;
        ram.buff[i + 1] = t;
        ram.buff[i + 2] = o;
        Opcode::OpFX33 { x }
    }

    fn op_fx55(&mut self, ram: &mut Ram, x: usize) -> Opcode {
        let start = self.i_reg.into();
        ram.load_data(start, &self.v_regs[0..=x]);
        Opcode::OpFX55 { x }
    }

    fn op_fx65(&mut self, ram: &Ram, x: usize) -> Opcode {
        let start = self.i_reg.into();
        let end = start + x;
        self.v_regs[0..=x].copy_from_slice(ram.buff[start..=end].as_ref());
        Opcode::OpFX65 { x }
    }
}

#[cfg(test)]
mod opcodes {

    use rand::Rng;

    use super::{Cpu, Opcode, REG_COUNT};

    use crate::{keyboard::Keyboard, ram::Ram, screen::Screen, stack::Stack};

    const ROM_ADDR: usize = 0x200;

    #[test]
    fn test_op_00e0() {
        let ret_opcode = Cpu::new(0).op_00e0(&mut Screen::new());
        assert!(matches!(ret_opcode, Opcode::Op00E0));
    }

    #[test]
    fn test_op_00ee() {
        let mut cpu = Cpu::new(0);
        let mut stack = Stack::new();

        // Fill stack
        stack.push(0, ROM_ADDR);
        cpu.sp = 1;

        let ret_opcode = cpu.op_00ee(&stack);

        assert_eq!(cpu.pc, ROM_ADDR);
        assert_eq!(cpu.sp, 0);
        assert!(matches!(ret_opcode, Opcode::Op00EE));
    }

    #[test]
    fn test_op_1nnn() {
        let nnn = rand::thread_rng().gen_range(0..=0xFFF);
        let opcode: u16 = 0x1000 | nnn;
        let mut cpu = Cpu::new(0);

        let ret_opcode = cpu.op_1nnn(opcode);

        assert_eq!(cpu.pc, nnn.into());
        assert!(matches!(ret_opcode, Opcode::Op1NNN { .. }));
    }

    #[test]
    fn test_op_2nnn() {
        let nnn = rand::thread_rng().gen_range(0..=0xFFF);
        let opcode: u16 = 0x2000 | nnn;
        let mut stack = Stack::new();
        let mut cpu = Cpu::new(ROM_ADDR);

        let ret_opcode = cpu.op_2nnn(&mut stack, opcode);

        assert_eq!(cpu.sp, 1);
        assert_eq!(cpu.pc, nnn.into());
        assert_eq!(stack.pop(cpu.sp - 1), ROM_ADDR);
        assert!(matches!(ret_opcode, Opcode::Op2NNN { .. }));
    }

    #[test]
    fn test_op_3xnn() {
        let x: u16 = rand::thread_rng().gen_range(0..16);
        let nn: u8 = rand::random();
        let opcode: u16 = 0x3000 | (x << 8) | u16::from(nn);
        let mut cpu = Cpu::new(0);

        cpu.v_regs[usize::from(x)] = nn;

        let ret_opcode = cpu.op_3xnn(x.into(), opcode);
        assert_eq!(cpu.pc, 2);
        assert!(matches!(ret_opcode, Opcode::Op3XNN { .. }));
    }

    #[test]
    fn test_op_4xnn() {
        let x: u16 = rand::thread_rng().gen_range(0..16);
        let nn: u8 = rand::random();
        let opcode: u16 = 0x3000 | (x << 8) | u16::from(nn);
        let mut cpu = Cpu::new(0);

        cpu.v_regs[usize::from(x)] = nn;

        let ret_opcode = cpu.op_4xnn(x.into(), opcode);
        assert_eq!(cpu.pc, 0);
        assert!(matches!(ret_opcode, Opcode::Op4XNN { .. }));
    }

    #[test]
    fn test_op_5xy0() {
        let x = rand::thread_rng().gen_range(0..16);
        let y = rand::thread_rng().gen_range(0..16);
        let data = rand::random();
        let mut cpu = Cpu::new(0);
        cpu.v_regs[x] = data;
        cpu.v_regs[y] = data;
        let ret_code = cpu.op_5xy0(x, y);
        assert_eq!(cpu.pc, 2, "{{x: {x}, y: {y}}}");
        assert!(matches!(ret_code, Opcode::Op5XY0 { .. }));
    }

    #[test]
    fn test_op_6xnn() {
        let x = rand::thread_rng().gen_range(0..16);
        let nn: u8 = rand::thread_rng().gen_range(0..=0xFF);
        let opcode: u16 = 0x6000 | x << 8 | u16::from(nn);
        let mut cpu = Cpu::new(0);
        let ret_code = cpu.op_6xnn(x.into(), opcode).unwrap();
        assert_eq!(cpu.v_regs[usize::from(x)], nn);
        assert!(matches!(ret_code, Opcode::Op6XNN { .. }));
    }

    #[test]
    fn test_op_7xnn() {
        let x = rand::thread_rng().gen_range(0..16);
        let nn: u8 = rand::thread_rng().gen_range(0..0xFF);
        let opcode: u16 = 0x7000 | x << 8 | u16::from(nn);
        let mut cpu = Cpu::new(0);
        cpu.v_regs[usize::from(x)] = 1;
        let ret_code = cpu.op_7xnn(x.into(), opcode).unwrap();

        assert_eq!(cpu.v_regs[usize::from(x)], nn + 1);
        assert!(matches!(ret_code, Opcode::Op7XNN { .. }))
    }

    #[test]
    fn test_op_8xy0() {
        let x = 0;
        let y = 1;
        let mut cpu = Cpu::new(0);

        cpu.v_regs[y] = 10;

        let ret_opcode = cpu.op_8xy0(x, y);

        assert_eq!(cpu.v_regs[x], cpu.v_regs[y]);
        assert!(matches!(ret_opcode, Opcode::Op8XY0 { .. }));
    }

    #[test]
    fn test_op_8xy1() {
        let x = 0;
        let y = 1;
        let mut cpu = Cpu::new(0);

        cpu.v_regs[x] = 0b1110_0000;
        cpu.v_regs[y] = 0b0000_0111;

        let ret_opcode = cpu.op_8xy1(x, y);

        assert_eq!(cpu.v_regs[x], 0b1110_0111);
        assert!(matches!(ret_opcode, Opcode::Op8XY1 { .. }));
    }

    #[test]
    fn test_op_8xy2() {
        let x = 0;
        let y = 1;
        let mut cpu = Cpu::new(0);

        cpu.v_regs[x] = 0b0101_1010;
        cpu.v_regs[y] = 0b1010_1101;

        let ret_opcode = cpu.op_8xy2(x, y);

        assert_eq!(cpu.v_regs[x], 0b0000_1000);
        assert!(matches!(ret_opcode, Opcode::Op8XY2 { .. }));
    }

    #[test]
    fn test_op_8xy3() {
        let x = 0;
        let y = 1;
        let mut cpu = Cpu::new(0);

        cpu.v_regs[x] = 0b0101_1010;
        cpu.v_regs[y] = 0b1010_1101;

        let ret_opcode = cpu.op_8xy3(x, y);

        assert_eq!(cpu.v_regs[x], 0b1111_0111);
        assert!(matches!(ret_opcode, Opcode::Op8XY3 { .. }));
    }

    #[test]
    fn test_op_8xy4() {
        for _ in 0..u16::MAX {
            let x = 0;
            let y = 1;
            let mut cpu = Cpu::new(0);

            cpu.v_regs[x] = rand::random();
            cpu.v_regs[y] = rand::random();

            let sum = cpu.v_regs[x].wrapping_add(cpu.v_regs[y]);
            let cond = (usize::from(cpu.v_regs[x]) + usize::from(cpu.v_regs[y])) > 255;

            let ret_opcode = cpu.op_8xy4(x, y);

            assert_eq!(cpu.v_regs[x], sum);
            assert_eq!(cpu.v_regs[0xF], Into::<u8>::into(cond));
            assert!(matches!(ret_opcode, Opcode::Op8XY4 { .. }));
        }
    }

    #[test]
    fn test_op_8xy5() {
        for _ in 0..u16::MAX {
            let x = 0;
            let y = 1;

            let mut cpu = Cpu::new(0);

            cpu.v_regs[x] = rand::random();
            cpu.v_regs[y] = rand::random();

            let cond: u8 = (cpu.v_regs[x] > cpu.v_regs[y]).into();
            let result = cpu.v_regs[x].wrapping_sub(cpu.v_regs[y]);

            let ret_opcode = cpu.op_8xy5(x, y);

            assert_eq!(result, cpu.v_regs[x]);
            assert_eq!(cond, cpu.v_regs[0xF]);
            assert!(matches!(ret_opcode, Opcode::Op8XY5 { .. }))
        }
    }

    #[test]
    fn test_op_8xy6() {
        for _ in 0..u16::MAX {
            let x = 0;
            let mut cpu = Cpu::new(0);

            let num = rand::random();

            cpu.v_regs[x] = num;

            let ret_opcode = cpu.op_8xy6(x);

            assert_eq!(cpu.v_regs[0xF], num & 0b0000_0001);
            assert_eq!(cpu.v_regs[x], num >> 1);
            assert!(matches!(ret_opcode, Opcode::Op8XY6 { .. }));
        }
    }

    #[test]
    fn test_op_8xy7() {
        for _ in 0..u16::MAX {
            let x = 0;
            let y = 1;
            let mut cpu = Cpu::new(0);

            cpu.v_regs[x] = rand::random();
            cpu.v_regs[y] = rand::random();

            let cond: u8 = (cpu.v_regs[y] > cpu.v_regs[x]).into();
            let result = cpu.v_regs[y].wrapping_sub(cpu.v_regs[x]);

            let ret_opcode = cpu.op_8xy7(x, y);

            assert_eq!(result, cpu.v_regs[x]);
            assert_eq!(cond, cpu.v_regs[0xF]);
            assert!(matches!(ret_opcode, Opcode::Op8XY7 { .. }));
        }
    }

    #[test]
    fn test_op_8xye() {
        for _ in 0..u16::MAX {
            let x = 0;
            let mut cpu = Cpu::new(0);

            let num = rand::random();

            cpu.v_regs[x] = num;

            let ret_opcode = cpu.op_8xye(x);

            assert_eq!(cpu.v_regs[0xF], num & 0b1000_0000);
            assert_eq!(cpu.v_regs[x], num << 1);
            assert!(matches!(ret_opcode, Opcode::Op8XYE { .. }));
        }
    }

    #[test]
    fn test_op_9xy0() {
        let x = rand::thread_rng().gen_range(0..16);
        let y = rand::thread_rng().gen_range(0..16);
        let data = rand::random();
        let mut cpu = Cpu::new(0);
        cpu.v_regs[x] = data;
        cpu.v_regs[y] = data;
        let ret_code = cpu.op_9xy0(x, y);
        assert_eq!(cpu.pc, 0, "{{x: {x}, y: {y}}}");
        assert!(matches!(ret_code, Opcode::Op9XY0 { .. }));
    }

    #[test]
    fn test_op_annn() {
        let nnn: u16 = rand::thread_rng().gen_range(0..=0xFFF);
        let opcode = 0xA000 | nnn;
        let mut cpu = Cpu::new(0);

        let ret_code = cpu.op_annn(opcode);

        assert_eq!(cpu.i_reg, nnn);
        assert!(matches!(ret_code, Opcode::OpANNN { .. }));
    }

    #[test]
    fn test_op_bnnn() {
        let nnn: u16 = rand::thread_rng().gen_range(0..=0xFFF);
        let opcode = 0xB000 | nnn;
        let mut cpu = Cpu::new(0);

        let ret_code = cpu.op_bnnn(opcode);

        assert_eq!(cpu.pc, (nnn + u16::from(cpu.v_regs[0])).into());
        assert!(matches!(ret_code, Opcode::OpBNNN { .. }));
    }

    #[test]
    fn test_op_cxnn() {
        let x: u16 = rand::thread_rng().gen_range(0..=0xF);
        let nn: u16 = rand::random();
        let opcode = 0xC000 | x << 8 | nn;
        let mut cpu = Cpu::new(0);
        let ret_code = cpu.op_cxnn(x.into(), opcode).unwrap();

        assert!(matches!(ret_code, Opcode::OpCXNN { .. }));
    }

    // #[test]
    // fn test_op_dxyn() {
    // 	let x = rand::thread_rng().gen_range(0..=0xF);
    // 	let y = rand::thread_rng().gen_range(0..=0xF);
    // 	let n: u8 = rand::random();
    // 	let mut screen = Screen::new();
    // 	let ram = Ram::new();
    // 	let mut cpu = Cpu::new(0);

    // 	let ret_code = cpu.op_dxyn(&ram, &mut screen, x, y, n);

    // 	assert!(matches!(ret_code, Opcode::OpDXYN { .. }));
    // 	todo!("Too lazy to implement this test.");
    // }

    #[test]
    fn test_op_ex9e() {
        let pc_pos = 0;
        let x = rand::thread_rng().gen_range(0..=0xF);
        let key_num = rand::thread_rng().gen_range(0..=0xF);
        let mut keyboard = Keyboard::new();
        let mut cpu = Cpu::new(pc_pos);
        cpu.v_regs[x] = key_num;
        keyboard.key_buf[usize::from(key_num)] = true;
        let ret_code = cpu.op_ex9e(&keyboard, x);
        assert_eq!(pc_pos + 2, cpu.pc);
        assert!(matches!(ret_code, Opcode::OpEX9E { .. }));
    }

    #[test]
    fn test_op_exa1() {
        let pc_pos = 0;
        let x = rand::thread_rng().gen_range(0..=0xF);
        let key_num = rand::thread_rng().gen_range(0..=0xF);
        let mut keyboard = Keyboard::new();
        let mut cpu = Cpu::new(pc_pos);
        cpu.v_regs[x] = key_num;
        keyboard.key_buf[usize::from(key_num)] = false;
        let ret_code = cpu.op_exa1(&keyboard, x);
        assert_eq!(pc_pos + 2, cpu.pc);
        assert!(matches!(ret_code, Opcode::OpEXA1 { .. }));
    }

    #[test]
    fn test_op_fx07() {
        let x = rand::thread_rng().gen_range(0..REG_COUNT);
        let mut cpu = Cpu::new(0);
        cpu.dt = rand::random();

        let ret_opcode = cpu.op_fx07(x);

        assert_eq!(cpu.dt, cpu.v_regs[x]);
        assert!(matches!(ret_opcode, Opcode::OpFX07 { .. }));
    }

    #[test]
    fn test_op_fx0a() {
        let x = rand::thread_rng().gen_range(0..REG_COUNT);
        let key_num = rand::thread_rng().gen_range(0..=0xF);
        let mut keyboard = Keyboard::new();
        let mut cpu = Cpu::new(2);
        keyboard.key_buf[key_num] = true;
        let ret_opcode = cpu.op_fx0a(&keyboard, x).unwrap();

        assert_eq!(key_num, cpu.v_regs[x].into());
        assert!(matches!(ret_opcode, Opcode::OpFX0A { .. }));
    }

    #[test]
    fn test_op_fx15() {
        let x = rand::thread_rng().gen_range(0..REG_COUNT);
        let mut cpu = Cpu::new(0);

        cpu.v_regs[x] = rand::random();

        let ret_opcode = cpu.op_fx15(x);

        assert_eq!(cpu.v_regs[x], cpu.dt);
        assert!(matches!(ret_opcode, Opcode::OpFX15 { .. }));
    }

    #[test]
    fn test_op_fx18() {
        let x = rand::thread_rng().gen_range(0..REG_COUNT);
        let mut cpu = Cpu::new(0);
        cpu.st = rand::random();

        let ret_opcode = cpu.op_fx18(x);

        assert_eq!(cpu.st, cpu.v_regs[x]);
        assert!(matches!(ret_opcode, Opcode::OpFX18 { .. }));
    }

    #[test]
    fn test_op_fx1e() {
        let x = rand::thread_rng().gen_range(0..REG_COUNT);
        let mut cpu = Cpu::new(0);

        cpu.i_reg = rand::random();
        cpu.v_regs[x] = rand::random();
        let sum: u16 = cpu.i_reg + u16::from(cpu.v_regs[x]);

        let ret_opcode = cpu.op_fx1e(x);

        assert_eq!(sum, cpu.i_reg);
        assert!(matches!(ret_opcode, Opcode::OpFX1E { .. }));
    }

    #[test]
    fn test_op_fx29() {
        let x = rand::thread_rng().gen_range(0..REG_COUNT);
        let mut cpu = Cpu::new(0);

        cpu.v_regs[x] = rand::random();
        let sprite_pos: u16 = u16::from(cpu.v_regs[x]) * 5;

        let ret_opcode = cpu.op_fx29(x);

        assert_eq!(cpu.i_reg, sprite_pos);
        assert!(matches!(ret_opcode, Opcode::OpFX29 { .. }));
    }

    #[test]
    fn test_op_fx33() {
        let x = rand::thread_rng().gen_range(0..REG_COUNT);
        let mut ram = Ram::new();
        let mut cpu = Cpu::new(0);

        cpu.v_regs[x] = rand::random();
        let ret_opcode = cpu.op_fx33(&mut ram, x);

        let i: usize = cpu.i_reg.into();

        let num = ram.buff[i] * 100 + ram.buff[i + 1] * 10 + ram.buff[i + 2];

        assert_eq!(cpu.v_regs[x], num);
        assert!(matches!(ret_opcode, Opcode::OpFX33 { .. }));
    }

    #[test]
    fn test_op_fx55() {
        let x = rand::thread_rng().gen_range(0..REG_COUNT);
        let mut ram = Ram::new();
        let mut cpu = Cpu::new(0);

        let i_reg: usize = cpu.i_reg.into();

        let ret_code = cpu.op_fx55(&mut ram, x);

        for i in 0..=x {
            assert_eq!(ram.buff[i_reg + i], cpu.v_regs[i]);
        }

        assert!(matches!(ret_code, Opcode::OpFX55 { .. }));
    }

    #[test]
    fn test_op_fx65() {
        let x = rand::thread_rng().gen_range(0..REG_COUNT);
        let ram = Ram::new();
        let mut cpu = Cpu::new(0);

        let i_reg: usize = cpu.i_reg.into();

        let ret_code = cpu.op_fx65(&ram, x);

        for i in 0..=x {
            assert_eq!(cpu.v_regs[i], ram.buff[i_reg + i]);
        }

        assert!(matches!(ret_code, Opcode::OpFX65 { .. }));
    }
}
