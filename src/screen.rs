pub const SCREEN_WIDTH: usize = 64;
pub const SCREEN_HEIGHT: usize = 32;

pub(super) struct Screen {
    pub(super) buff: [bool; SCREEN_WIDTH * SCREEN_HEIGHT],
    pub(super) update_flag: bool,
}

impl Screen {
    pub(super) const fn new() -> Self {
        Self {
            buff: [false; SCREEN_WIDTH * SCREEN_HEIGHT],
            update_flag: true,
        }
    }

    pub(super) fn clear(&mut self) {
        *self = Self::new();
    }
}
