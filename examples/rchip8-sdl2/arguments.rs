use std::path::PathBuf;

use clap::{command, crate_name, Arg, ArgAction};

pub(super) struct Args {
    pub(super) window_name: String,
    pub(super) rom_file: PathBuf,
    pub(super) instructions_per_second: u64,
    pub(super) scale: usize,
    pub(super) verbose: bool,
}

impl Args {
    pub(super) fn new(default_ips: u64, default_scale: usize) -> Self {
        let arg_matches = command!()
            .args([
                Arg::new("ROM_FILE")
                    .short('r')
                    .long("rom")
                    .action(ArgAction::Set)
                    .required(true)
                    .help("ROM file to be executed."),
                Arg::new("IPS")
                    .short('i')
                    .long("instructions-per-second")
                    .action(ArgAction::Set)
                    .help(format!(
					"Emulation velocity in instruction per seconds (IPS). Defaults to {default_ips}.",
				)),
                Arg::new("SCALE")
                    .short('s')
                    .long("scale")
                    .action(ArgAction::Set)
                    .help(format!(
                        "Scale factor. Defaults to {}, ({}x{}).",
                        default_scale,
                        default_scale * rchip8::SCREEN_WIDTH,
                        default_scale * rchip8::SCREEN_HEIGHT
                    )),
                Arg::new("VERBOSITY")
                    .short('v')
                    .long("verbose")
                    .action(ArgAction::SetTrue)
                    .help("Display assembly instructions"),
            ])
            .get_matches();

        let file_path_str = arg_matches.get_one::<String>("ROM_FILE").expect("required");

        Self {
            window_name: format!("{} - {}", crate_name!(), file_path_str),
            rom_file: file_path_str.into(),
            instructions_per_second: *arg_matches.get_one::<u64>("IPS").unwrap_or(&default_ips),
            scale: *arg_matches
                .get_one::<usize>("SCALE")
                .unwrap_or(&default_scale),
            verbose: arg_matches.get_flag("VERBOSITY"),
        }
    }
}
