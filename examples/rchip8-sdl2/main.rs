use app::App;

mod app;
mod arguments;
mod events;
mod renderer;
mod sound;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    App::new()?.start()?;
    Ok(())
}
