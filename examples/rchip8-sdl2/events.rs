use sdl2::{event::Event, keyboard::Keycode, EventPump, Sdl};

use rchip8::{Chip8, Keys};

pub struct Events(EventPump);

impl Events {
    pub fn new(sdl_ctxt: &Sdl) -> Result<Self, String> {
        Ok(Self(sdl_ctxt.event_pump()?))
    }

    pub fn poll(&mut self, system: &mut Chip8) -> bool {
        for event in self.0.poll_iter() {
            match event {
                Event::Quit { .. } => {
                    return false;
                }
                Event::KeyDown {
                    keycode: Some(key), ..
                } => match key {
                    Keycode::Num1 => system.press_key(Keys::K1),
                    Keycode::Num2 => system.press_key(Keys::K2),
                    Keycode::Num3 => system.press_key(Keys::K3),
                    Keycode::Num4 => system.press_key(Keys::KC),
                    Keycode::Q => system.press_key(Keys::K4),
                    Keycode::W => system.press_key(Keys::K5),
                    Keycode::E => system.press_key(Keys::K6),
                    Keycode::R => system.press_key(Keys::KD),
                    Keycode::A => system.press_key(Keys::K7),
                    Keycode::S => system.press_key(Keys::K8),
                    Keycode::D => system.press_key(Keys::K9),
                    Keycode::F => system.press_key(Keys::KE),
                    Keycode::Z => system.press_key(Keys::KA),
                    Keycode::X => system.press_key(Keys::K0),
                    Keycode::C => system.press_key(Keys::KB),
                    Keycode::V => system.press_key(Keys::KF),
                    Keycode::Space => system.reset(),
                    Keycode::Escape => {
                        return false;
                    }
                    _ => (),
                },
                Event::KeyUp {
                    keycode: Some(key), ..
                } => match key {
                    Keycode::Num1 => system.release_key(Keys::K1),
                    Keycode::Num2 => system.release_key(Keys::K2),
                    Keycode::Num3 => system.release_key(Keys::K3),
                    Keycode::Num4 => system.release_key(Keys::KC),
                    Keycode::Q => system.release_key(Keys::K4),
                    Keycode::W => system.release_key(Keys::K5),
                    Keycode::E => system.release_key(Keys::K6),
                    Keycode::R => system.release_key(Keys::KD),
                    Keycode::A => system.release_key(Keys::K7),
                    Keycode::S => system.release_key(Keys::K8),
                    Keycode::D => system.release_key(Keys::K9),
                    Keycode::F => system.release_key(Keys::KE),
                    Keycode::Z => system.release_key(Keys::KA),
                    Keycode::X => system.release_key(Keys::K0),
                    Keycode::C => system.release_key(Keys::KB),
                    Keycode::V => system.release_key(Keys::KF),
                    _ => (),
                },
                _ => (),
            };
        }
        true
    }
}
