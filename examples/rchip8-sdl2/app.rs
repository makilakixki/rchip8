use std::{
    fs::File,
    io::{self, Read},
    path::PathBuf,
    time::{SystemTime, UNIX_EPOCH},
};

use rchip8::Chip8;

use crate::{arguments::Args, events::Events, renderer::Renderer, sound::Sound};

const DEFAULT_INSTRUCTIONS_PER_SECOND: u64 = 1000;
const DEFAULT_SCALE: usize = 10;

pub(super) struct App {
    renderer: Renderer,
    sound: Sound,
    events: Events,
    system: Chip8,
    rom_file: PathBuf,
    verbose: bool,
}

impl App {
    pub(super) fn new() -> Result<Self, String> {
        let sdl_ctxt = sdl2::init()?;
        let args = Args::new(DEFAULT_INSTRUCTIONS_PER_SECOND, DEFAULT_SCALE);
        Ok(Self {
            renderer: Renderer::new(&sdl_ctxt, &args.window_name, args.scale)?,
            sound: Sound::new(&sdl_ctxt)?,
            events: Events::new(&sdl_ctxt)?,
            system: Chip8::new(args.instructions_per_second),
            rom_file: args.rom_file,
            verbose: args.verbose,
        })
    }

    pub(super) fn start(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        self.load_rom_file()?;
        self.emu_loop()?;
        Ok(())
    }

    fn emu_loop(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        while self.events.poll(&mut self.system) {
            let (maybe_opcode, maybe_beep_state) = self
                .system
                .tick(SystemTime::now().duration_since(UNIX_EPOCH)?.as_nanos())?;
            if self.system.get_screen_update_flag() {
                self.renderer.render(&self.system).unwrap();
                self.system.set_screen_as_drawn();
            }

            if let Some(beep_state) = maybe_beep_state {
                if beep_state {
                    self.sound.play();
                } else {
                    self.sound.pause();
                }
            }

            if self.verbose {
                if let Some(opcode) = maybe_opcode {
                    println!("{opcode}");
                }
            }
        }
        Ok(())
    }

    fn load_rom_file(&mut self) -> Result<(), io::Error> {
        let rom = {
            let mut file = File::open(&self.rom_file)?;
            let mut rom = Vec::new();
            file.read_to_end(&mut rom).unwrap();
            rom
        };
        self.system.load_rom(&rom).unwrap();
        Ok(())
    }
}
