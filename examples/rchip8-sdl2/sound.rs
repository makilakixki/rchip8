use sdl2::audio::{AudioCallback, AudioDevice, AudioSpecDesired, AudioStatus};
use sdl2::Sdl;

struct SquareWave {
    phase_inc: f32,
    phase: f32,
    volume: f32,
}

impl AudioCallback for SquareWave {
    type Channel = f32;

    fn callback(&mut self, out: &mut [f32]) {
        for x in out.iter_mut() {
            *x = if self.phase <= 0.5 {
                self.volume
            } else {
                -self.volume
            };
            self.phase = (self.phase + self.phase_inc) % 1.0;
        }
    }
}

pub struct Sound(AudioDevice<SquareWave>);

impl Sound {
    pub fn new(sdl_ctxt: &Sdl) -> Result<Self, String> {
        let audio = sdl_ctxt.audio()?;

        let desired_spec = AudioSpecDesired {
            freq: Some(44100),
            channels: Some(1),
            samples: None,
        };

        let device = audio.open_playback(None, &desired_spec, |spec| SquareWave {
            phase_inc: 440.0 / spec.freq as f32,
            phase: 0.0,
            volume: 0.25,
        })?;
        Ok(Self(device))
    }

    pub fn play(&self) {
        match self.0.status() {
            AudioStatus::Paused | AudioStatus::Stopped => {
                self.0.resume();
            }
            _ => (),
        }
    }

    pub fn pause(&self) {
        if let AudioStatus::Playing = self.0.status() {
            self.0.pause();
        }
    }
}
