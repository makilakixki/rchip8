use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::Canvas;
use sdl2::video::Window;
use sdl2::Sdl;

use rchip8::{Chip8, SCREEN_HEIGHT, SCREEN_WIDTH};

pub struct Renderer {
    canvas: Canvas<Window>,
    scale: usize,
}

impl Renderer {
    pub fn new(sdl_ctxt: &Sdl, window_title: &str, scale: usize) -> Result<Self, String> {
        let video = sdl_ctxt.video()?;
        let window = video
            .window(
                window_title,
                (SCREEN_WIDTH * scale) as u32,
                (SCREEN_HEIGHT * scale) as u32,
            )
            .position_centered()
            .build();
        let window = match window {
            Ok(window) => window,
            Err(err) => {
                return Err(err.to_string());
            }
        };

        let canvas = match window.into_canvas().build() {
            Ok(canvas) => canvas,
            Err(err) => {
                return Err(err.to_string());
            }
        };

        Ok(Self { canvas, scale })
    }

    pub fn render(&mut self, system: &Chip8) -> Result<(), String> {
        for i in 0..SCREEN_WIDTH {
            for j in 0..SCREEN_HEIGHT {
                if system.get_pixel(i, j) {
                    self.canvas.set_draw_color(Color::GREEN);
                } else {
                    self.canvas.set_draw_color(Color::BLUE);
                }

                self.canvas.fill_rect(Rect::new(
                    (i * self.scale) as i32,
                    (j * self.scale) as i32,
                    self.scale as u32,
                    self.scale as u32,
                ))?;
            }
        }
        self.canvas.present();
        Ok(())
    }
}
